﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Primera Práctica
Para la resolución de esta primera práctica se utilizará como herramienta de concurrencia los semáforos. Para el análisis y diseño el uso de semáforos es como se ha presentado en las clases de teoría. Para la implementación se hará uso de la clase [`Semaphore`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Semaphore.html) de JAVA. Hay diferentes ejemplos en este [guión](https://gitlab.com/ssccdd/materialadicional/-/blob/master/README.md) donde se demuestra el uso general de la clase así como soluciones de cursos anteriores. Para la implementación se utilizará la factoría [`Executors`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Executors.html) y la interface [`ExecutorService`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas concurrentes que compondrán la solución de la práctica.

## Problema: Sistema de Procesamiento de Pedidos
Vamos a simular un proceso de venta por Internet. Para la simulación deberemos resolver una serie de tareas que deberán ejecutarse de forma concurrente garantizando una máxima eficiencia. 

1.  **RecibirPedido**: Recibe pedidos de los clientes.
2.  **VerificarPago**: Verifica y confirma el pago del pedido.
3.  **ChequearInventario**: Verifica la disponibilidad de los artículos pedidos.
4.  **PrepararPedido**: Empaqueta los artículos una vez que el pago ha sido verificado y los artículos están confirmados como disponibles.
5.  **OrganizarEnvio**: Organiza el envío del paquete al cliente.
6.  **ActualizarInventario**: Actualiza el inventario basado en los artículos enviados.
7.  **NotificarCliente**: Notifica al cliente sobre el estado de su pedido.
8. **EntregarPedido** : El cliente recibe el pedido del transportista y valorará el servicio de envío así como la calidad del producto que ha comprado.

En las diferentes tareas estarán implicados una serie de procesos que deberán utilizar semáforos para garantizar el uso correcto de los datos y la sincronización de las tareas. Los procesos son los siguientes:

 1. proceso **Usuario** : Simula las acciones que deberá seguir el usuario para comprar los articulos que desee en la tienda. Para simplificar el proceso las solicitudes de los artículos se hacen a una tienda que dispone de ellos en su catálogo.
 2. proceso **Tienda** : Tiene un catálogo de productos que desea vender a los clientes.
 3. proceso **Transportista** : Será el encargado de recibir los encargos de transporte de la tienda para enviarlos al cliente correspondiente.
 4. proceso **Proveedor** : Será el encargado de atender las necesidades de inventario de la tienda para completar los productos de su catálogo.

### Para la implementación

Deberán definirse tiempos que simulen las operaciones que tienen que hacer los diferentes procesos implicados en el problema. También deberá definirse un tiempo para la finalización de la prueba. Hay que mostrar información en la consola para seguir la ejecución de los procesos.

El hilo principal deberá crear un número suficiente de procesos para demostrar la concurrencia de los mismo. De todos los procesos deberá haber instancias suficientes.

Además debe implementarse un proceso de finalización que se lanzará pasado un tiempo que estará definido en las constantes. Por tanto todos los procesos presentes en la solución deberán implementar su interrupción como el método de finalización.
